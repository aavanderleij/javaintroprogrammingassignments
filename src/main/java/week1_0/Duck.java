package week1_0;

/**
 * Creation date: Jun 27, 2017
 *
 * @version 0.01
 * @autor Michiel Noback (&copy; 2017)
 */
public class Duck {
    public int swimSpeed;   //default is 0
    public String name;     //default is NULL
}
